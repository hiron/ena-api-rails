namespace :api do 
    namespace :v1 do 
        resources :transactions, only: [:index, :destroy, :create]
        resources :vehicles, only: [:index, :update, :destroy, :show, :create]

        get "/fitness/:day", to:"fitness#show", as: :fitness
        get "/tax/:day", to:"tax#show", as: :tax
        get "/route/:day", to:"route#show", as: :route
        get "/insurance/:day", to:"insurance#show", as: :insurance
        

        scope :signup, module: :users do 
            post '/', to: 'registrations#create', as: :user_registration
        end
    end
end

scope :api do 
    scope :v1 do 
        use_doorkeeper do 
            skip_controllers :authorizations, :applications, :authorized_applications
        end
    end
end

