class CreateVehicles < ActiveRecord::Migration[7.0]
  def change
    create_table :vehicles do |t|
      t.string :vehicle_number, null: false, index: {unique: true}
      t.string :owner
      t.date :tax, null: false
      t.date :fitness, null: false
      t.date :route_permit, null: false
      t.date :insurance, null: false

      t.timestamps
    end
  end
end
