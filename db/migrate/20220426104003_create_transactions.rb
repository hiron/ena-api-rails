class CreateTransactions < ActiveRecord::Migration[7.0]
  def change
    create_table :transactions do |t|
      t.integer :type, null: false
      # t.references :user, null: false, foreign_key: true
      # t.references :vehicle, null: false, foreign_key: true

      t.timestamps
    end

    # add_foreign_key :transactions, :users, column: :user_id
    # add_foreign_key :transactions, :vehicles, column: :vehicle_id
  end
end
