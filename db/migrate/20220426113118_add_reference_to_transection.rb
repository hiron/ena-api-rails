class AddReferenceToTransection < ActiveRecord::Migration[7.0]
  def change
    add_reference :transactions, :vehicles, null: false, foreign_key: true
    add_reference :transactions, :users, null: false, foreign_key: true
  end
end
