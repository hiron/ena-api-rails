class AddOldroutepermitToVehicle < ActiveRecord::Migration[7.0]
  def change
    add_column :vehicles, :old_route_permit, :date
  end
end
