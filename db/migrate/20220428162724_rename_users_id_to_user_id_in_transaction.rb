class RenameUsersIdToUserIdInTransaction < ActiveRecord::Migration[7.0]
  def change
    rename_column :transactions, :users_id, :user_id
    rename_column :transactions, :vehicles_id, :vehicle_id
  end
end
