require 'swagger_helper'

describe "Ena API" do 
    path '/api/v1/signup' do
        post 'Create a User' do 
            tags 'Registration'
            consumes 'application/json'
            parameter name: :signup, in: :body, schema: { 
                type: :object,
                properties: {
                    email: { type: :string, default: "hcdas@outlook.com"},
                    password: { type: :string, default:"password" },
                    client_id: { type: :string,  default: "VooFKiPVUU3ks4JQXwqx0Li8jyXyu1Iz8rgPhBHIPc0"},
                },
                required: ['email', 'password', 'client_id']
             }

            response '200', 'user created' do 
                after do |example|
                    example.metadata[:response][:content]['application/json'][:examples] = {
                        example_1: {
                          value: JSON.parse(response.body, symbolize_names: true),
                          summary: "Summary (optional)",
                          description: "Description (Optional)"
                        }
                      }
                end
                run_test!
            end
            response '422', 'Unprocessable Entity' do
                # let(:Authorization) { "Bearer #{::Base64.strict_encode64('bogus:bogus')}" }
                run_test!
            end
        end
    end
    path '/api/v1/oauth/token' do
        post 'Login' do 
            tags 'Registration'
            consumes 'application/json'
            parameter name: :signin, in: :body, schema: { 
                type: :object,
                properties: {
                    grant_type:{ type: :string, default: "password" },
                    email: { type: :string, default: "hcdas@outlook.com"},
                    password: { type: :string, default:"password" },
                    client_id: { type: :string,  default: "Z5p8GC_5kPsdog2mvA9xp6bCZf8T5dzkRDoO5Bd0hZk"},
                    client_secret:{type: :string, "default"=> "_Lbm8zQmCATTlsdbNYQWSj-JhYL6QfbzwiZPctEMVcg"}
                   
                    
                },
                required: ['email', 'password', 'client_id',"client_secret", "grant_type"]
             }

            response '200', 'Login successful' do 
                after do |example|
                    example.metadata[:response][:content]['application/json'][:examples] = {
                        example_1: {
                          value: JSON.parse(response.body, symbolize_names: true),
                          summary: "Summary (optional)",
                          description: "Description (Optional)"
                        }
                      }
                end
                run_test!
            end

            response '422', 'Unprocessable Entity' do
                # let(:Authorization) { "Bearer #{::Base64.strict_encode64('bogus:bogus')}" }
                run_test!
            end
        end
    end

    path '/api/v1/oauth/revoke' do
        post 'Logout' do 
            tags 'Registration'
            consumes 'application/json'
            parameter name: :signout, in: :body, schema: { 
                type: :object,
                properties: {
                    "token": { type: :string },
                     "client_id": { type: :string },
                    "client_secret": { type: :string }
                },
                required: ['token', 'client_id',"client_secret"]
             }

            response '200', 'Logout successfully' do 
                after do |example|
                    example.metadata[:response][:content]['application/json'][:examples] = {
                        example_1: {
                          value: JSON.parse(response.body, symbolize_names: true),
                          summary: "Summary (optional)",
                          description: "Description (Optional)"
                        }
                      }
                end
                run_test!
            end

            response '403', 'Forbidden Entity' do
                # let(:Authorization) { "Bearer #{::Base64.strict_encode64('bogus:bogus')}" }
                run_test!
            end
        end
    end


    path '/api/v1/vehicles' do
        post 'Add new Vehicle' do 
            tags 'Vehicles'
            consumes 'application/json'
            produces 'application/json'
            operationId 'testBearer'
            security [token: ['write']]
            parameter name: :Authorization, in: :header, required: true, type: :string, description: "Client Token"
            parameter name: :add_vehicle, in: :body, schema: { 
                type: :object,
                properties: {
                    "vehicle_number"=> { type: :string, default: "D-M-ch-00-1234" },
                    "owner"=> { type: :string, default: "Ena transport" },
                    "tax"=> { type: :date, default: "12 apr 2022" },
                    "route_permit"=> { type: :date, default: "14 apr 2022" },
                    "fitness"=> { type: :date, default: "12 may 2022" },
                    "insurance"=> { type: :date, default: "12 aug 2022" }
                },
                required: ['vehicle_number', 'tax',"route_permit", "fitness", "insurance"]
             }

            response '201', 'New Vehicle is added' do 
                # after do |example|
                #     example.metadata[:response][:content]['application/json'][:examples] = {
                #         example_1: {
                #           value: JSON.parse(response.body, symbolize_names: true),
                #           summary: "Summary (optional)",
                #           description: "Description (Optional)"
                #         }
                #       }
                # end
                let(:Authorization) { "Bearer #{generate_token}" }
                
                run_test!
            end

            response '403', 'Forbidden Entity' do
                # let(:Authorization) { "Bearer #{::Base64.strict_encode64('bogus:bogus')}" }
                run_test!
            end
            response '401', 'Unauthorized Entity' do
                # let(:Authorization) { "Bearer #{::Base64.strict_encode64('bogus:bogus')}" }
                run_test!
            end
        end
    
        get 'Get All Vehicles' do
            tags 'Vehicles'
            produces 'application/json'
            # operationId 'testBearer'
            security [token: ['read']]
            parameter name: :Authorization, in: :header, required: true, type: :string, description: "Client Token"

            response '200', 'Get all vehicles' do 
                # example 'application/json', :example_key, [{
                #     id: 1,
                #     vehicle_number: 'D-M-CH-123-76',
                #     owner: 'Citi Bank', 
                #     tax: "12 may 2022",
                #     route_permit: "12 may 2022",
                #     fitness: "12 may 2022",
                #     insurance:"12 may 2022"
                # }]
                schema type: :array,
                properties: {
                    "id" => {type: :integer},
                    "vehicle_number"=> { type: :string,  },
                    "owner"=> { type: :string,  },
                    "tax"=> { type: :date, },
                    "route_permit"=> { type: :date, },
                    "fitness"=> { type: :date, },
                    "insurance"=> { type: :date, }
                },
                required: ['vehicle_number', 'tax',"route_permit", "fitness", "insurance"]
                
                let(:Authorization) { "Bearer #{generate_token}" }
                run_test!
            end

            response '401', 'Unauthorized Entity' do
                # let(:Authorization) { "Bearer #{::Base64.strict_encode64('bogus:bogus')}" }
                run_test!
            end
        end
    end

    path "/api/v1/vehicles/{id}" do
        get 'Get a Vehicle' do
            tags 'Vehicles'
            produces 'application/json'
            # operationId 'testBearer'
            security [token: ['read']]
            parameter name: :Authorization, in: :header, required: true, type: :string, description: "Client Token"
            parameter name: :id, in: :path, required: true, type: :string, description: "Vehicle Id"

            response '200', 'Get a vehicle details' do 
                # example 'application/json', :example_key, [{
                #     id: 1,
                #     vehicle_number: 'D-M-CH-123-76',
                #     owner: 'Citi Bank', 
                #     tax: "12 may 2022",
                #     route_permit: "12 may 2022",
                #     fitness: "12 may 2022",
                #     insurance:"12 may 2022"
                # }]
                schema type: :object,
                properties: {
                    "id" => {type: :integer},
                    "vehicle_number"=> { type: :string, default: "D-M-ch-00-1234" },
                    "owner"=> { type: :string, default: "City Bank" },
                    "tax"=> { type: :date, default: "12 apr 2022" },
                    "route_permit"=> { type: :date, default: "14 apr 2022" },
                    "fitness"=> { type: :date, default: "12 may 2022" },
                    "insurance"=> { type: :date, default: "12 aug 2022" }
                },
                required: ['vehicle_number', 'tax',"route_permit", "fitness", "insurance"]
                
                let(:Authorization) { "Bearer #{generate_token}" }
                let(:id) {Vehicle.first.id}
                run_test!
            end

            response '401', 'Unauthorized Entity' do
                # let(:Authorization) { "Bearer #{::Base64.strict_encode64('bogus:bogus')}" }
                run_test!
            end
        end

        put 'Update a Vehicle' do
            tags 'Vehicles'
            produces 'application/json'
            # operationId 'testBearer'
            security [token: ['write']]
            parameter name: :id, in: :path, required: true, type: :string, description: "Vehicle Id"
            parameter name: :Authorization, in: :header, required: true, type: :string, description: "Client Token"
            parameter required: true, in: :body, schema: { 
                type: :object,
                properties: {
                    "vehicle_number"=> { type: :string, default: "D-M-ch-00-1234" },
                    "owner"=> { type: :string, default: "Ena transport" },
                    "tax"=> { type: :date, default: "12 apr 2022" },
                    "route_permit"=> { type: :date, default: "14 apr 2022" },
                    "fitness"=> { type: :date, default: "12 may 2022" },
                    "insurance"=> { type: :date, default: "12 aug 2022" }
                },
                required: ['vehicle_number', 'tax',"route_permit", "fitness", "insurance"]
             }
            response '200', 'vehicle is updated' do 
                # example 'application/json', :example_key, [{
                #     id: 1,
                #     vehicle_number: 'D-M-CH-123-76',
                #     owner: 'Citi Bank', 
                #     tax: "12 may 2022",
                #     route_permit: "12 may 2022",
                #     fitness: "12 may 2022",
                #     insurance:"12 may 2022"
                # }]
                schema type: :object,
                properties: {
                    "id" => {type: :integer},
                    "vehicle_number"=> { type: :string, default: "D-M-ch-00-1234" },
                    "owner"=> { type: :string, default: "City Bank" },
                    "tax"=> { type: :date, default: "12 apr 2022" },
                    "route_permit"=> { type: :date, default: "14 apr 2022" },
                    "fitness"=> { type: :date, default: "12 may 2022" },
                    "insurance"=> { type: :date, default: "12 aug 2022" },
                    sold: {type: :boolean, default: false}
                },
                required: ['vehicle_number', 'tax',"route_permit", "fitness", "insurance"]
                
                let(:Authorization) { "Bearer #{generate_token}" }
                let(:id) {Vehicle.first.id}
                run_test!
            end

            response '401', 'Unauthorized Entity' do
                # let(:Authorization) { "Bearer #{::Base64.strict_encode64('bogus:bogus')}" }
                run_test!
            end
        end

        delete 'sold a Vehicle' do
            tags 'Vehicles'
            produces 'application/json'
            # operationId 'testBearer'
            security [token: ['write']]
            parameter name: :id, in: :path, required: true, type: :string, description: "Vehicle Id"
            parameter name: :Authorization, in: :header, required: true, type: :string, description: "Client Token"

            response '200', 'Vehicle is sold' do 
                # example 'application/json', :example_key, [{
                #     id: 1,
                #     vehicle_number: 'D-M-CH-123-76',
                #     owner: 'Citi Bank', 
                #     tax: "12 may 2022",
                #     route_permit: "12 may 2022",
                #     fitness: "12 may 2022",
                #     insurance:"12 may 2022"
                # }]
                schema type: :object,
                properties: {
                    "id" => {type: :integer},
                    "vehicle_number"=> { type: :string, default: "D-M-ch-00-1234" },
                    "owner"=> { type: :string, default: "City Bank" },
                    "tax"=> { type: :date, default: "12 apr 2022" },
                    "route_permit"=> { type: :date, default: "14 apr 2022" },
                    "fitness"=> { type: :date, default: "12 may 2022" },
                    "insurance"=> { type: :date, default: "12 aug 2022" },
                    sold: {type: :boolean, default: true}
                },
                required: ['vehicle_number', 'tax',"route_permit", "fitness", "insurance"]
                
                let(:Authorization) { "Bearer #{generate_token}" }
                let(:id) {Vehicle.first.id}
                run_test!
            end

            response '401', 'Unauthorized Entity' do
                # let(:Authorization) { "Bearer #{::Base64.strict_encode64('bogus:bogus')}" }
                run_test!
            end
        end
    end
    path "/api/v1/route/{day}" do 
        get 'Get all Vehicles from current date to given or passing days' do
            tags 'Vehicles'
            produces 'application/json'
            # operationId 'testBearer'
            security [token: ['read']]
            parameter name: :day, in: :path, required: true, type: :string, description: "Vehicle Id"
            parameter name: :Authorization, in: :header, required: true, type: :string, description: "Client Token"

            response '200', 'Get all Vehicles which route permit date will be expired between {day} days' do 
                # example 'application/json', :example_key, [{
                #     id: 1,
                #     vehicle_number: 'D-M-CH-123-76',
                #     owner: 'Citi Bank', 
                #     tax: "12 may 2022",
                #     route_permit: "12 may 2022",
                #     fitness: "12 may 2022",
                #     insurance:"12 may 2022"
                # }]
                schema type: :array,
                properties: {
                    "id" => {type: :integer},
                    "vehicle_number"=> { type: :string, default: "D-M-ch-00-1234" },
                    "owner"=> { type: :string, default: "City Bank" },
                    "tax"=> { type: :date, default: "12 apr 2022" },
                    "route_permit"=> { type: :date, default: "14 apr 2022" },
                    "fitness"=> { type: :date, default: "12 may 2022" },
                    "insurance"=> { type: :date, default: "12 aug 2022" },
                    sold: {type: :boolean, default: true}
                },
                required: ['vehicle_number', 'tax',"route_permit", "fitness", "insurance"]
                
                let(:Authorization) { "Bearer #{generate_token}" }
                let(:day) {12}
                run_test!
            end

            response '401', 'Unauthorized Entity' do
                # let(:Authorization) { "Bearer #{::Base64.strict_encode64('bogus:bogus')}" }
                run_test!
            end
        end
    end

    path "/api/v1/tax/{day}" do 
        get 'Get all Vehicles from current date to given/passing days' do
            tags 'Vehicles'
            produces 'application/json'
            # operationId 'testBearer'
            security [token: ['read']]
            parameter name: :day, in: :path, required: true, type: :string, description: "Vehicle Id"
            parameter name: :Authorization, in: :header, required: true, type: :string, description: "Client Token"

            response '200', 'Get all Vehicles which tax token date will be expired within {day} days ' do 
                # example 'application/json', :example_key, [{
                #     id: 1,
                #     vehicle_number: 'D-M-CH-123-76',
                #     owner: 'Citi Bank', 
                #     tax: "12 may 2022",
                #     route_permit: "12 may 2022",
                #     fitness: "12 may 2022",
                #     insurance:"12 may 2022"
                # }]
                schema type: :array,
                properties: {
                    "id" => {type: :integer},
                    "vehicle_number"=> { type: :string, default: "D-M-ch-00-1234" },
                    "owner"=> { type: :string, default: "City Bank" },
                    "tax"=> { type: :date, default: "12 apr 2022" },
                    "route_permit"=> { type: :date, default: "14 apr 2022" },
                    "fitness"=> { type: :date, default: "12 may 2022" },
                    "insurance"=> { type: :date, default: "12 aug 2022" },
                    sold: {type: :boolean, default: true}
                },
                required: ['vehicle_number', 'tax',"route_permit", "fitness", "insurance"]
                
                let(:Authorization) { "Bearer #{generate_token}" }
                let(:day) {12}
                run_test!
            end

            response '401', 'Unauthorized Entity' do
                # let(:Authorization) { "Bearer #{::Base64.strict_encode64('bogus:bogus')}" }
                run_test!
            end
        end
    end

    path "/api/v1/insurance/{day}" do 
        get 'Get all Vehicles from current date to given/passing days' do
            tags 'Vehicles'
            produces 'application/json'
            # operationId 'testBearer'
            security [token: ['read']]
            parameter name: :day, in: :path, required: true, type: :string, description: "Vehicle Id"
            parameter name: :Authorization, in: :header, required: true, type: :string, description: "Client Token"

            response '200', 'Get all Vehicles which insurance date will be expired within {day} days' do 
                # example 'application/json', :example_key, [{
                #     id: 1,
                #     vehicle_number: 'D-M-CH-123-76',
                #     owner: 'Citi Bank', 
                #     tax: "12 may 2022",
                #     route_permit: "12 may 2022",
                #     fitness: "12 may 2022",
                #     insurance:"12 may 2022"
                # }]
                schema type: :array,
                properties: {
                    "id" => {type: :integer},
                    "vehicle_number"=> { type: :string, default: "D-M-ch-00-1234" },
                    "owner"=> { type: :string, default: "City Bank" },
                    "tax"=> { type: :date, default: "12 apr 2022" },
                    "route_permit"=> { type: :date, default: "14 apr 2022" },
                    "fitness"=> { type: :date, default: "12 may 2022" },
                    "insurance"=> { type: :date, default: "12 aug 2022" },
                    sold: {type: :boolean, default: true}
                },
                required: ['vehicle_number', 'tax',"route_permit", "fitness", "insurance"]
                
                let(:Authorization) { "Bearer #{generate_token}" }
                let(:day) {12}
                run_test!
            end

            response '401', 'Unauthorized Entity' do
                # let(:Authorization) { "Bearer #{::Base64.strict_encode64('bogus:bogus')}" }
                run_test!
            end
        end
    end

    path "/api/v1/fitness/{day}" do 
        get 'Get all Vehicles from current date to given/passing days' do
            tags 'Vehicles'
            produces 'application/json'
            # operationId 'testBearer'
            security [token: ['read']]
            parameter name: :day, in: :path, required: true, type: :string, description: "Vehicle Id"
            parameter name: :Authorization, in: :header, required: true, type: :string, description: "Client Token"

            response '200', 'Get all Vehicles which fitness certificate date will be expired within {day} days' do 
                # example 'application/json', :example_key, [{
                #     id: 1,
                #     vehicle_number: 'D-M-CH-123-76',
                #     owner: 'Citi Bank', 
                #     tax: "12 may 2022",
                #     route_permit: "12 may 2022",
                #     fitness: "12 may 2022",
                #     insurance:"12 may 2022"
                # }]
                schema type: :array,
                properties: {
                    "id" => {type: :integer},
                    "vehicle_number"=> { type: :string, default: "D-M-ch-00-1234" },
                    "owner"=> { type: :string, default: "City Bank" },
                    "tax"=> { type: :date, default: "12 apr 2022" },
                    "route_permit"=> { type: :date, default: "14 apr 2022" },
                    "fitness"=> { type: :date, default: "12 may 2022" },
                    "insurance"=> { type: :date, default: "12 aug 2022" },
                    sold: {type: :boolean, default: true}
                },
                required: ['vehicle_number', 'tax',"route_permit", "fitness", "insurance"]
                
                let(:Authorization) { "Bearer #{generate_token}" }
                let(:day) {12}
                run_test!
            end

            response '401', 'Unauthorized Entity' do
                # let(:Authorization) { "Bearer #{::Base64.strict_encode64('bogus:bogus')}" }
                run_test!
            end
        end
    end

    path "/api/v1/transactions" do 
        get 'Get all fee Transactions history' do
            tags 'Transactions'
            produces 'application/json'
            # operationId 'testBearer'
            security [token: ['read']]
            parameter name: :Authorization, in: :header, required: true, type: :string, description: "Client Token"

            response '200', 'Get all Transactions' do 
                schema type: :array,
                properties: {
                    "id" => {type: :integer},
                    "vehicle_number"=> { type: :string, default: "D-M-ch-00-1234" },
                    "email"=> { type: :string, default: "test@gmail.com" },
                    transaction_type: {type: :integer, default: 0},
                    created_at: {type: :dateTime, default: "Sat, 14 May 2022 16:28:30.914881000 UTC +00:00"},
                    updated_at: {type: :dateTime, default: "Sat, 14 May 2022 16:28:30.914881000 UTC +00:00"},
                    vehicle_id: {type: :integer},
                    user_id: {type: :integer},
                },
                required: ['vehicle_number', 'tax',"route_permit", "fitness", "insurance"]
                
                let(:Authorization) { "Bearer #{generate_token}" }
                run_test!
            end

            response '401', 'Unauthorized Entity' do
                # let(:Authorization) { "Bearer #{::Base64.strict_encode64('bogus:bogus')}" }
                run_test!
            end
        end

        post 'Create new fee Transactions' do
            tags 'Transactions'
            produces 'application/json'
            consumes 'application/json'
            # operationId 'testBearer'
            security [token: ['write']]
            parameter name: :Authorization, in: :header, required: true, type: :string, description: "Client Token"
            parameter in: :body, required: true, schema: {
                type: :object,
                properties: {
                    transaction_type: {type: :integer, default: 1},
                    date: {type: :date, default: "Sat, 14 May 2022"},
                    vehicle_id: {type: :integer},
                },
                required: ['transaction_type', 'date',"vehicle_id"]
            }
            response '201', 'Get new created Transactions' do 
                schema type: :object,
                properties: {
                    "id" => {type: :integer},
                    "vehicle_number"=> { type: :string, default: "D-M-ch-00-1234" },
                    "email"=> { type: :string, default: "test@gmail.com" },
                    transaction_type: {type: :integer, default: 0},
                    created_at: {type: :dateTime, default: "Sat, 14 May 2022 16:28:30.914881000 UTC +00:00"},
                    updated_at: {type: :dateTime, default: "Sat, 14 May 2022 16:28:30.914881000 UTC +00:00"},
                    vehicle_id: {type: :integer},
                    user_id: {type: :integer},
                },
                required: ['transaction_type', 'date',"vehicle_id"]
                let(:Authorization) { "Bearer #{generate_token}" }
                run_test!
            end

            response '401', 'Unauthorized Entity' do
                # let(:Authorization) { "Bearer #{::Base64.strict_encode64('bogus:bogus')}" }
                run_test!
            end
        end
    end

    path "/api/v1/transactions/{id}" do 
        delete 'Undo Transactions' do
            tags 'Transactions'
            produces 'application/json'
            # operationId 'testBearer'
            security [token: ['write']]
            parameter name: :Authorization, in: :header, required: true, type: :string, description: "Client Token"
            parameter name: :id, in: :path, required: true, type: :string, description: "Transaction id"

            response '200', 'remove a Transactions' do 
                schema type: :object,
                properties: {notice: {type: :string, default: "Transaction was successfully destroyed."}}

                let(:Authorization) { "Bearer #{generate_token}" }
                let(:id) {"1"}
                run_test!
            end

            response '401', 'Unauthorized Entity' do
                # let(:Authorization) { "Bearer #{::Base64.strict_encode64('bogus:bogus')}" }
                run_test!
            end
        end
    end

end