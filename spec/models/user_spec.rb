require 'rails_helper'

RSpec.describe User, type: :model do
  # pending "add some examples to (or delete) #{__FILE__}"
  context "Validataion tests" do 
    it "ensures Email presence" do 
      user = User.new(password: "password")
      expect(user).not_to be_valid
    end
    
    it "email format is wrong" do 
      user = User.new(password: "password", email: "test.com")
      expect(user).not_to be_valid
      # expect(user).to be_valid
    end

    it "email format is correct" do 
      user = User.new(password: "password", email: "hieon@test.com")
      expect(user).to be_valid
    end

    it "ensures password presence" do 
      user = User.new( email: "hieon@test.com").save
      expect(user).to eq(false)
    end
    it "user is authentic" do 
      User.create!( email: "hieon@test.com", password:"123456")
      
      expect(User.authenticate("hieon@test.com", "123456")).not_to be_nil
    end
  end
end
