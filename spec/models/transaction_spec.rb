require 'rails_helper'

RSpec.describe Transaction, type: :model do
  context "validation test" do 
    it "ensure type presence" do
      # puts Transaction.types[:tax]
      transaction = Transaction.new(transaction_type: Transaction.types[:tax], vehicle_id: 1)
      expect(transaction).to be_valid
    end
  end
end
