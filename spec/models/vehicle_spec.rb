require 'rails_helper'

RSpec.describe Vehicle, type: :model do
  let(:vehicle_valid){{
    vehicle_number: "D-M-ka-00-1234", 
    owner: "City Bank", 
    tax: "12 mar 2022", 
    route_permit: "21 Apr 2022",
    fitness: "27 may 2022",
    insurance: "30 Apr 2022"
   }}

  context "Validation Tests" do
    it "ensure vehicle No. presence" do
        vehicle = Vehicle.new(vehicle_valid.except( :vehicle_number))
        expect(vehicle).not_to be_valid
    end

    it "ensure tax presence" do
      vehicle = Vehicle.new(vehicle_valid.except( :tax))
      expect(vehicle).not_to be_valid
    end

    it "ensure route permit presence" do
      vehicle = Vehicle.new(vehicle_valid.except( :route_permit))
      expect(vehicle).not_to be_valid
    end

    it "ensure fitness presence" do
      vehicle = Vehicle.new(vehicle_valid.except( :fitness))
      expect(vehicle).not_to be_valid
    end

    it "ensure insurance presence" do
      vehicle = Vehicle.new(vehicle_valid.except( :insurance))
      expect(vehicle).not_to be_valid
    end
  end
end
