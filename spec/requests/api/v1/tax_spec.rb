require 'rails_helper'

RSpec.describe "Api::V1::Taxes", type: :request do
  before(:all){ 
    application = Doorkeeper::Application.first!
    loginUser = {
      client_id: application.uid,
      client_secret: application.secret,
      email: "hiron@gmail.com", 
      password: "password", 
      grant_type: "password"
    } 
    post '/api/v1/oauth/token', params: loginUser, headers: {ACCEPT: "application/json"}
    puts response.body
    @token = JSON.parse(response.body)
    # puts @token["access_token"]

    Vehicle.find_or_create_by!(vehicle_number: "D-M-ka-00-1234", 
      owner: "City Bank", 
      tax: "28 apr 2022", 
      fitness: "10 apr 2022",
      route_permit: "21 Apr 2022",
      insurance: "30 Apr 2022")

      Vehicle.find_or_create_by!(vehicle_number: "D-M-GA-00-1234", 
        owner: "Islami Bank", 
        tax: "25 apr 2022", 
        fitness: "10 may 2022",
        route_permit: "21 Apr 2022",
        insurance: "30 Apr 2022")

        Vehicle.find_or_create_by!(vehicle_number: "D-M-LA-00-1234", 
          owner: "Ena", 
          tax: "27 apr 2022", 
          fitness: "02 may 2022",
          route_permit: "21 Apr 2022",
          insurance: "30 Apr 2022")


          Vehicle.find_or_create_by!(vehicle_number: "D-M-ch-00-1234", 
            owner: "IFIC Bank", 
            tax: "25 apr 2022", 
            fitness: "15 may 2022",
            route_permit: "21 Apr 2022",
            insurance: "30 Apr 2022")
  }

  let(:header) do {ACCEPT: "application/json", Authorization: "bearer #{@token["access_token"]}"} end
  
  describe "GET /show" do 
    it "returns vehicles of certain days" do 
      get api_v1_tax_url(15), headers: header
      expect(response.status).to eq(200)
      puts JSON.parse(response.body).size
    end


    it "returns vehicles of expired date" do 
      get api_v1_tax_url(0), headers: header
      expect(response.status).to eq(200)
      puts JSON.parse(response.body).size
    end
  end
end
