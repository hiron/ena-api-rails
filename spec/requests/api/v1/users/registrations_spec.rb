require 'rails_helper'

RSpec.describe "Api::V1::Users::Registrations", type: :request do
  let (:application) do
    Doorkeeper::Application.first!
  end
  let(:valid_request) {
    {
      client_id: application.uid,
      email: "test@gmail.com", 
      password: "password"
    } 
  }

  let (:header){ {ACCEPT: "application/json"}}

  describe "POST /signup" do
    # pending "add some examples (or delete) #{__FILE__}"
    it "create new user" do 
      post '/api/v1/signup', params: valid_request, headers: header
      expect(response.status).to eq(200)
      expect(response.content_type).to eq("application/json; charset=utf-8")
    end
  end
end
