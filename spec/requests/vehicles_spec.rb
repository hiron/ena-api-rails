require 'rails_helper'

RSpec.describe "/vehicles", type: :request do

  # let (:header){ {ACCEPT: "application/json"} }

  before(:all){ 
    application = Doorkeeper::Application.first!
    loginUser = {
      client_id: application.uid,
      client_secret: application.secret,
      email: "hiron@gmail.com", 
      password: "password", 
      grant_type: "password"
    } 
    post '/api/v1/oauth/token', params: loginUser, headers: {ACCEPT: "application/json"}
    puts response.body
    @token = JSON.parse(response.body)
    # puts @token["access_token"]
  }
  
  let(:valid_attributes) {
    {
      vehicle_number: "D-M-ka-19-1234", 
      owner: "City Bank", 
      tax: "12 mar 2022".to_date, 
      route_permit: "21 Apr 2022".to_date,
      fitness: "27 may 2022".to_date,
      insurance: "30 Apr 2022".to_date
     }
  }

  let(:invalid_attributes) {
    {
      vehicle_number: "D-M-ka-18-1234", 
      owner: "City Bank", 
      tax: "aere", 
      fitness: "sfwrr",
      route_permit: "21 Apr 2022",
      insurance: "30 Apr 2022"
     }
  }
  


  describe "GET /index" do
    it "renders a successful response" do
      vehicle = Vehicle.create! valid_attributes
      get api_v1_vehicles_path, headers: {ACCEPT: "application/json", Authorization: "bearer #{@token["access_token"]}"}
      expect(response.status).to be(200)
      puts response.body
      vehicle.destroy
    end
  end

  describe "GET /show" do
    it "renders a successful response" do
      vehicle = Vehicle.create! valid_attributes
      get api_v1_vehicle_path(vehicle), headers: {ACCEPT: "application/json", Authorization: "bearer #{@token["access_token"]}"}
      expect(response.status).to be(200)
      puts response.body
      vehicle.destroy
    end
  end

  describe "POST /create" do
    context "with valid parameters" do
      it "creates a new Vehicle" do
        # puts "I am here #{@token}"
        post '/api/v1/vehicles', params: valid_attributes, headers: {ACCEPT: "application/json", Authorization: "bearer #{@token["access_token"]}"}
        expect(response.status).to be(201)
        Vehicle.find_by(vehicle_number: valid_attributes[:vehicle_number]).destroy
      end
    end

    context "with invalid parameters" do
      it "does not create a new Vehicle" do
        post '/api/v1/vehicles', params: invalid_attributes, headers: {ACCEPT: "application/json", Authorization: "bearer #{@token["access_token"]}"}
        puts response.body
        expect(response.status).to be(422)
      end
    end
  end

  describe "PATCH /update" do
    context "with valid parameters" do
      let(:new_attributes) {
        {
          vehicle_number: "D-M-ka-40-1234", 
          owner: "Ena", 
          tax: "12 mar 2022".to_date, 
          route_permit: "21 Apr 2022".to_date,
          fitness: "27 may 2022".to_date,
          insurance: "30 Apr 2022".to_date
         }
      }

      it "updates the requested vehicle" do
        vehicle = Vehicle.create! valid_attributes
        put api_v1_vehicle_url(vehicle), params: new_attributes, headers: {ACCEPT: "application/json", Authorization: "bearer #{@token["access_token"]}"}
        vehicle.reload
        expect(response.status).to eq(200)
        puts(response.body)
        vehicle.destroy
      end
    end

    context "with invalid parameters" do
      it "updates the requested vehicle Fail" do
        vehicle = Vehicle.create! valid_attributes
        patch api_v1_vehicle_url(vehicle), params: invalid_attributes, headers: {ACCEPT: "application/json", Authorization: "bearer #{@token["access_token"]}"}
        expect(response.status).to eq(422)
        puts(response.body)
        vehicle.destroy
      end
    end
  end

  describe "DELETE /destroy" do
    it "destroys the requested vehicle" do
      vehicle = Vehicle.create! valid_attributes
     
      delete api_v1_vehicle_url(vehicle), headers: {ACCEPT: "application/json", Authorization: "bearer #{@token["access_token"]}"}
      expect(response.status).to eq(200)
      puts response.body
      vehicle.destroy
    end
  end
end
