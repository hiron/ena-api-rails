require 'rails_helper'

RSpec.describe "/transactions", type: :request do
  
  before(:all){ 
    application = Doorkeeper::Application.first!
    loginUser = {
      client_id: application.uid,
      client_secret: application.secret,
      email: "hiron@gmail.com", 
      password: "password", 
      grant_type: "password"
    } 
    post '/api/v1/oauth/token', params: loginUser, headers: {ACCEPT: "application/json"}
    puts response.body
    @token = JSON.parse(response.body)
    # puts @token["access_token"]
  }

  let(:header) do 
    {ACCEPT: "application/json", Authorization: "bearer #{@token["access_token"]}"}
  end


  let(:valid_attributes) {
    {
      user_id: 1,
      vehicle_id: Vehicle.first.id, 
      transaction_type: Transaction.types[:fitness]
    }
  }

  let(:invalid_attributes) {
    {
      transaction_type: Transaction.types[:fitness]
    }
  }

  describe "GET /index" do
    it "renders a successful response" do
      Transaction.create! valid_attributes
      get api_v1_transactions_url, headers: header
      expect(response.status).to be(200)
      puts response.body
    end
  end

  # describe "GET /show" do
  #   it "renders a successful response" do
  #     transaction = Transaction.create! valid_attributes
  #     get transaction_url(transaction), headers: header
  #     expect(response.status).to be(200)
  #     puts response.body
  #   end
  # end

  # describe "GET /new" do
  #   it "renders a successful response" do
  #     get new_transaction_url
  #     expect(response).to be_successful
  #   end
  # end

  # describe "GET /edit" do
  #   it "renders a successful response" do
  #     transaction = Transaction.create! valid_attributes
  #     get edit_transaction_url(transaction)
  #     expect(response).to be_successful
  #   end
  # end

  describe "POST /create" do
    context "with valid parameters" do
      it "creates a new Transaction" do
        post api_v1_transactions_url, params: valid_attributes.except(:users_id), headers: header
        expect(response.status).to be(201)
        puts response.body
      end
    end

    context "with invalid parameters" do
      it "does not create a new Transaction" do
          post api_v1_transactions_url, params: invalid_attributes, headers: header
          expect(response.status).to be(422)
          puts response.body
      end
    end
  end


  describe "DELETE /destroy" do
    it "destroys the requested transaction" do
      transaction = Transaction.create! valid_attributes
      delete api_v1_transaction_url(transaction), headers: header
      expect(response.status).to be(200)
    end

    # it "redirects to the transactions list" do
    #   transaction = Transaction.create! valid_attributes
    #   delete transaction_url(transaction)
    #   expect(response).to redirect_to(transactions_url)
    # end
  end
end
