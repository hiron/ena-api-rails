# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

## Ruby Version

    ruby "2.7.6" 

## System DEPENDENCIES

    Docker version 20.10.14, build a224086

## Configuration

Copy this ```docker-compose.yml``` file

 ```yml
version: '3.3'
services:
  db:
    image: postgres:11
    ports:
      - 54320:5432
    volumes:
      - postgres:/var/lib/postgresql/data
    environment:
      POSTGRES_PASSWORD: <your_password> #give your database pass here

  ena-api-rails:
    image: registry.gitlab.com/hiron/ena-api-rails:v1.0
    ports:
      - 3000:3000
    environment:
      - DATABASE_URL=postgresql://postgres:<your_password>@db:5432/postgres
    # give your database pass &
    # your database username here


volumes:
  postgres:

 ```

## Database creation

After saving ```docker-compose.yml``` file, run these commands

```sh
docker-compose up
```

```sh
docker exec -it ena_api_rails-web-1 bash
```

If the bash is open then run the ```rails``` command to create database

```sh
rails db:create
```

## Database initialization

```sh
rails db:migrate
rails db:seed
```

## How to run the test suite

```sh
docker exec -it ena_api_rails-web-1 bash
rspec
```

## Documentation

```sh 
rake rswag:specs:swaggerize
```

path: ```http://localhost:3000/api-docs```
