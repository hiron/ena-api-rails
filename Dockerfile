FROM ruby:2.7
RUN apt-get update -qq && apt-get install -y nodejs postgresql-client 
RUN mkdir /ena-api-rails 
WORKDIR /ena-api-rails 
COPY Gemfile /ena-api-rails/Gemfile
COPY Gemfile.lock /ena-api-rails/Gemfile.lock
RUN bundle install
COPY . /ena-api-rails

COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh 
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 3000
# RUN ["rake" "rswag:specs:swaggerize"]
CMD ["rails", "server", "-b", "0.0.0.0"]
