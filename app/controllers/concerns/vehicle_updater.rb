module VehicleUpdater
    extend ActiveSupport::Concern

    def update_vehicle_date(field, id, data = Date.current) 
        vehicle = Vehicle.find(id)
        if(field == 'route_permit')
            vehicle.update(old_route_permit: vehicle.route_permit)
            vehicle.update(route_permit: date)
        else
            vehicle.update("#{field}": vehicle["#{field}"] + 1.years)
        end
    end

    def undo_update_vehicle_date(field, id) 
        vehicle = Vehicle.find(id)
        if(field == 'route_permit')
            vehicle.update(route_permit: vehicle.old_route_permit)
        else
            vehicle.update("#{field}": vehicle["#{field}"] - 1.years)
        end
    end

end