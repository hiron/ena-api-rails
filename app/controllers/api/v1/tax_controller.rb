class Api::V1::TaxController < ApplicationController
    def show 
        if params[:day].to_i > 0 
            @vehicles = Vehicle.expired_by(params[:day], 'tax')
        else
            @vehicles = Vehicle.expired('tax')
        end
        render json: @vehicles, status: :ok
    end
end
