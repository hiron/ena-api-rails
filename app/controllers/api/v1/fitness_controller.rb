class Api::V1::FitnessController < ApiController
    def show 
        if params[:day].to_i > 0 
            @vehicles = Vehicle.expired_by(params[:day], 'fitness')
        else
            @vehicles = Vehicle.expired('fitness')
        end
        render json: @vehicles, status: :ok
    end

end
