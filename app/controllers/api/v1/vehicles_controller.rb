class Api::V1::VehiclesController < ApiController
  before_action :set_vehicle, only: %i[ show edit update destroy ]

  # GET /vehicles
  def index
    @vehicles = Vehicle.all.where(sold: false)
    render json: @vehicles, status: :ok
  end

  # GET /vehicles/1
  def show
    render json: @vehicle, status: :ok
  end

  # GET /vehicles/new
  # def new
  #   @vehicle = Vehicle.new
  # end

  # GET /vehicles/1/edit
  # def edit
  # end

  # POST /vehicles
  def create
    @vehicle = Vehicle.new(vehicle_params)

    if @vehicle.save
      render json: @vehicle, status: :created, notice: "Vehicle was successfully created."
    else
      render json: {error: @vehicle.errors}, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /vehicles/1
  def update
    if @vehicle.update(vehicle_params)
      render json:@vehicle, status: :ok, notice: "Vehicle was successfully updated."
    else
      render json: {error: @vehicle.errors}, status: :unprocessable_entity
    end
  end

  # DELETE /vehicles/1
  def destroy
    @vehicle.update(sold: true)
    render json: @vehicle, notice: "Vehicle was successfully Sold.", status: :ok
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vehicle
      @vehicle = Vehicle.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def vehicle_params
      params.permit(:vehicle_number, :owner, :tax, :fitness, :route_permit, :insurance)
    end
end
