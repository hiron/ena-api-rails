class Api::V1::InsuranceController < ApplicationController
    def show 
        if params[:day].to_i > 0 
            @vehicles = Vehicle.expired_by(params[:day], 'insurance')
        else
            @vehicles = Vehicle.expired('insurance')
        end
        render json: @vehicles, status: :ok
    end
end
