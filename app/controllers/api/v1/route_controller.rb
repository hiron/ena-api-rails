class Api::V1::RouteController < ApplicationController
  def show 
    if params[:day].to_i > 0 
        @vehicles = Vehicle.expired_by(params[:day], 'route_permit')
    else
        @vehicles = Vehicle.expired('route_permit')
    end
    render json: @vehicles, status: :ok
  end
end
