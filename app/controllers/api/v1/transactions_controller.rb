class Api::V1::TransactionsController < ApiController
  before_action :set_transaction, only: %i[ edit update destroy ]
  include VehicleUpdater

  # GET /transactions
  def index
    @transactions = Transaction.select('transactions.*', 'vehicles.vehicle_number','users.email').joins(:vehicle, :user).all.order(created_at: :desc)
    render json: @transactions, status: :ok
  end

  # GET /transactions/1
  # def show
  # end

  # GET /transactions/new
  # def new
  #   @transaction = Transaction.new
  # end

  # GET /transactions/1/edit
  # def edit
  # end

  # POST /transactions
  def create
    @transaction = Transaction.new(transaction_params)

    if @transaction.save

      if @transaction.tax!
          update_vehicle_date('tax', @transaction.vehicle_id)
          
      elsif @transaction.fitness!
          update_vehicle_date('fitness', @transaction.vehicle_id)
      elsif @transaction.route_permit!
          update_vehicle_date('route_permit', @transaction.vehicle_id, transaction_params[:date])
      elsif @transaction.insurance!
          update_vehicle_date('insurance', @transaction.vehicle_id)
      end

      render json: @transaction, status: :created, notice: "Transaction was successfully created."
    else
      render json: {error: @transaction.errors}, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /transactions/1
  # def update
  #   if @transaction.update(transaction_params)
  #     redirect_to @transaction, notice: "Transaction was successfully updated."
  #   else
  #     render :edit, status: :unprocessable_entity
  #   end
  # end

  # DELETE /transactions/1
  def destroy
  
    if @transaction.tax!
        undo_update_vehicle_date('tax', @transaction.vehicle_id)
    elsif @transaction.fitness!
        undo_update_vehicle_date('fitness', @transaction.vehicle_id)
    elsif @transaction.route_permit!
        undo_update_vehicle_date('route_permit', @transaction.vehicle_id, transaction_params[:date])
    elsif @transaction.insurance!
        undo_update_vehicle_date('insurance', @transaction.vehicle_id)
    end
    @transaction.destroy
    render json:{notice: "Transaction was successfully destroyed."}, status: :ok
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_transaction
      @transaction = Transaction.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def transaction_params
      params.permit(:transaction_type, :user_id, :vehicle_id, :date).reverse_merge!(user_id: current_user.id)
    end
end
