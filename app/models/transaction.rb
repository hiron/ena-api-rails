class Transaction < ApplicationRecord
  belongs_to :user, optional: true
  belongs_to :vehicle, ->{ where sold: false}, optional: true

  enum type: %i[tax fitness route_permit insurance]

  validates :transaction_type, presence: true
  validates :vehicle_id, presence: true
end
