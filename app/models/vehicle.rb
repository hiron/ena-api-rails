class Vehicle < ApplicationRecord
    # has_many :transactions, dependent: :destroy

    validates :vehicle_number, presence: true
    validates :tax, presence: true 
    validates :route_permit, presence: true
    validates :fitness, presence: true
    validates :insurance, presence: true

    scope :expired_by, ->(num, field) {where("#{field}": Date.current..(Date.current + num.to_i.day) )}
    scope :expired, ->(field) { where("#{field} < ?", Date.current ) }
end
